from pyramid.response import Response
from pyramid.view import view_config

from sqlalchemy.exc import DBAPIError

from .models import (
    DBSession,
    MyModel,
    )


@view_config(route_name='splash', renderer='templates/splash.jinja2')
def splash(request):
    """
    Sample view that passes in a 'title' variable to templates/splash.jinja2.
    The template also gets an implicit 'request' argument with all the methods
    available here:

       http://docs.pylonsproject.org/projects/pyramid/en/latest/api/request.html
    """
    return {'title': 'Healing Rain: Positively Gaming'}


"""
Here's a sample of a new view you might add:


  @view_config(route_name='profile', renderer='templates/profile.jinja2')
  def profile(request):
      return {}


Then you can use any valid html/xml/jinja2 markup in a file called
profile.jinja2 inside the templates directory, and in the template you can
obtain anything in the static folder with:

    {{ request.static_url('healingrain:static/path/to/whatever') }}

Next you'd add this to __init__.py with the other routes:

    config.add_route('profile', 'some/profile/route')


This only covers static pages but I can give better examples once we
start adding models to the database. There are also a lot of libraries and
patterns used by the pyramid community to avoid boilerplate that we can look at
anytime we start repeating ourselves, but I wanted to give enough basic
instructions that anyone could at least add a basic view and start playing with
it.
"""
